<?php

namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Utils;
use Grav\Common\Data\Data;
use Grav\Common\Page\Page;
use Grav\Common\GPM\Response;
use ZendXml\Exception\RuntimeException;

/**
 * Class GoogleMapsPlugin
 * @package Grav\Plugin
 */
class EtdMeteoPlugin extends Plugin
{

    const CACHE_TODAY_LIFETIME = 3600; // Une heure
    const CACHE_PREVISIONS_LIFETIME = 10800; // Trois heures

    const TEMPLATE_ETD_METEO_HTML = 'partials/etd-meteo-function.html.twig';
    const TEMPLATE_ETD_METEO_BUTTON_HTML = 'partials/etd-meteo-button.html.twig';

    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onShortcodeHandlers' => ['onShortcodeHandlers', 0],
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
        ];
    }

    public function onPluginsInitialized()
    {
        $this->enable([
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onTwigInitialized' => ['onTwigInitialized', 0]
        ]);
    }

    public function onTwigInitialized()
    {
        $this->grav['twig']->twig->addFunction(new \Twig_SimpleFunction('etd_meteo', [$this, 'etdMeteo']));
        $this->grav['twig']->twig->addFunction(new \Twig_SimpleFunction('etd_meteo_button', [$this, 'etdMeteoButton']));
    }


    public function onShortcodeHandlers()
    {
        $this->grav['shortcode']->registerAllShortcodes(__DIR__ . '/shortcodes');
    }

    public function onTwigTemplatePaths()
    {
        $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';
    }

    public function etdMeteo()
    {

        return $this->render(self::TEMPLATE_ETD_METEO_HTML, [
            "previsions" => $this->getPrevisions(),
            "today" => $this->getToday()
        ]);

    }

    public function etdMeteoButton()
    {

        return $this->render(self::TEMPLATE_ETD_METEO_BUTTON_HTML, [
            "today" => $this->getToday()
        ]);

    }

    protected function render($template, $vars)
    {
        return $this->grav['twig']->twig()->render($template, $vars);
    }

    public function getToday()
    {

        /**
         * @var $cache \Grav\Common\Cache
         */
        $cache = $this->grav['cache'];

        // Si la mise en cache est activée
        if ($cache->getEnabled()) {

            // Calcul de la clé
            $key = md5("etd-meteo:getToday");

            // On essaye de charger les infos depuis le cache
            if ($cache->contains($key)) {
                return $cache->fetch($key);
            }

            // On récupère les infos.
            $today = $this->_getToday();

            // On stocke les infos en cache
            $cache->save($key, $today, self::CACHE_TODAY_LIFETIME);

            return $today;

        }

        return $this->_getToday();

    }

    protected function _getToday()
    {

        $idCity = $this->grav['config']->get('plugins')['etd-meteo']['idCity'];
        $idApp = $this->grav['config']->get('plugins')['etd-meteo']['idApp'];
        $language = $this->grav['language']->getActive();
        try {

            return (array)json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?id=" . $idCity . "&units=metric&lang=" . $language . "&appid=" . $idApp . ""));

        } catch (\Exception $e) {

        }

        return false;

    }

    public function getPrevisions()
    {

        /**
         * @var $cache \Grav\Common\Cache
         */
        $cache = $this->grav['cache'];

        // Si la mise en cache est activée
        if ($cache->getEnabled()) {

            // Calcul de la clé
            $key = md5("etd-meteo:getPrevisions");

            // On essaye de charger les infos depuis le cache
            if ($cache->contains($key)) {
                return $cache->fetch($key);
            }

            // On récupère les infos.
            $previsons = $this->_getPrevisions();

            // On stocke les infos en cache
            $cache->save($key, $previsons, self::CACHE_PREVISIONS_LIFETIME);

            return $previsons;

        }

        return $this->_getPrevisions();

    }

    protected function _getPrevisions()
    {

        $previsions = [];
        $idCity = $this->grav['config']->get('plugins')['etd-meteo']['idCity'];
        $idApp = $this->grav['config']->get('plugins')['etd-meteo']['idApp'];
        $language = $this->grav['language']->getActive();
        try {
            $data = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/forecast?id=" . $idCity . "&units=metric&lang=" . $language . "&appid=" . $idApp . ""));

            foreach ($data->list AS $prevision) {
                if (!isset($previsions[substr($prevision->dt_txt, 0, 10)]['date'])) {
                    $previsions[substr($prevision->dt_txt, 0, 10)]['date'] = $prevision->dt;
                }
                if (!isset($previsions[substr($prevision->dt_txt, 0, 10)]['min']) || $previsions[substr($prevision->dt_txt, 0, 10)]['min'] > $prevision->main->temp) {
                    $previsions[substr($prevision->dt_txt, 0, 10)]['min'] = $prevision->main->temp;
                }

                if (!isset($previsions[substr($prevision->dt_txt, 0, 10)]['max']) || $previsions[substr($prevision->dt_txt, 0, 10)]['max'] < $prevision->main->temp) {
                    $previsions[substr($prevision->dt_txt, 0, 10)]['max'] = $prevision->main->temp;
                }
                if (!isset($previsions[substr($prevision->dt_txt, 0, 10)]['code']) || ($prevision->weather[0]->id != 800 && $prevision->sys->pod == 'd') || ($prevision->weather[0]->id < $previsions[substr($prevision->dt_txt, 0, 10)]['code'] && $prevision->weather[0]->id != 800 && $prevision->sys->pod == 'd'))
                    $previsions[substr($prevision->dt_txt, 0, 10)]['code'] = $prevision->weather[0]->id;
            }

            return $previsions;

        } catch (\Exception $e) {

        }

        return false;

    }

}
