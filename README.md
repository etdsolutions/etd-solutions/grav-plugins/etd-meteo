# Grav Meteo Plugin

## About

The **Meteo** plugin provides the Meteo as shortcode.

## Installation

Typically a plugin should be installed via [GPM](http://learn.getgrav.org/advanced/grav-gpm) (Grav Package Manager):

```
$ bin/gpm install meteo
```

Alternatively it can be installed via the [Admin Plugin](http://learn.getgrav.org/admin-panel/plugins)

## Configuration

```
enabled: true
```

## Quick Example

```
[meteo]

[/meteo]
```

## Available Parameters