<?php

namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;


class EtdMeteoShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('etd-meteo', function(ShortcodeInterface $sc) {

            $output = $this->twig->processTemplate('partials/etd-meteo.html.twig', []);

            return $output;
        });
    }
}
